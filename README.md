# USB to FTDI FT230XS Programmer 

This design is based on the [hello.serial-UPDI.FT230X](http://academy.cba.mit.edu/classes/embedded_programming/FTDI/USB-FT230XS-UPDI.jpg) by Neil Gerhenfeld as a part of the Fab Academy to program those ATtiny UPDI IC boards.

This speciffic board has a FTDI connector no the other end. You can use it in two ways.

1. To power your board while programming via UPDI
2. To power and program it if UPDI is integrated on the board you are programming. 

![Headshot](images/headshot.jpg)
![Headshot](images/pcb.png)
